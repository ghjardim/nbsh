//vim: ft=c ts=4 sw=4 et :
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

//TODO: I think it only works on glibc. Port it to POSIX.
void type_prompt(){
    char hostname[1024];
    hostname[1023]=0;
    gethostname(hostname, 1023);

    char username[1024];
    username[1023]=0;
    getlogin_r(username, 1023);

    printf("%s@%s $ ", username, hostname);
}

#define COMMAND_INITIAL_SIZE 10
#define REALLOC_RATE 10
char* read_command(){
    int size = COMMAND_INITIAL_SIZE;
    char* buffer = (char*) malloc(size*(sizeof(char)));

    int counter = 0;
    while((buffer[counter] = getc(stdin)) != '\n'){
        counter++;
        if(counter >= size){
            size = size + REALLOC_RATE;
            buffer = (char*) realloc(buffer, (size)*(sizeof(char)));
        }
    }
    buffer[counter] = 0;

    return buffer;
}

#define STRIPLIST_INITIAL_SIZE 10
char** strip_command(char* command){
    const char sep[] = " ";
    char* token;

    int size = STRIPLIST_INITIAL_SIZE;
    char** token_collection = (char**) malloc(size*(sizeof(char*)));
    int counter = 0;

    token = strtok(command, sep);
    token_collection[counter] = token;

    while(token != NULL){
        counter++;
        token_collection[counter] = token;

        if(counter >= size){
            size = size + REALLOC_RATE;
            token_collection = (char**) realloc(token_collection, (size)*(sizeof(char*)));
        }

        token = strtok(NULL, sep); //Moving forward
    }

    //Ending
    counter++;
    if(counter >= size){
        size = size + 1;
        token_collection = (char**) realloc(token_collection, (size)*(sizeof(char*)));
    }
    token_collection[counter] = NULL;

    return token_collection;
}

// The same as strip_command
// TODO: unify both functions
char** strip(char* command){
    char sep[] = ":";
    char* token;
    int size = STRIPLIST_INITIAL_SIZE;

    // Needed, because strtok overwrites its input
    char* tmpCommand = malloc(strlen(command)+1);
    strcpy(tmpCommand, command);

    char** token_collection = (char**) malloc(size*(sizeof(char*)));
    int counter = 0;

    token = strtok(tmpCommand, sep);
    token_collection[counter] = token;

    printf("%s \t", token_collection[counter]);

    while(token != NULL){
        counter++;
        token_collection[counter] = token;

        printf("%s \t", token_collection[counter]);

        if(counter >= size){
            size = size + REALLOC_RATE;
            token_collection = (char**) realloc(token_collection, (size)*(sizeof(char*)));
        }
        token = strtok(NULL, sep); //Moving forward
    }

    //Ending
    counter++;
    if(counter >= size){
        size = size + 1;
        token_collection = (char**) realloc(token_collection, (size)*(sizeof(char*)));
    }
    token_collection[counter] = NULL;

    printf("%s \t", token_collection[counter]);

    return token_collection;
}

void is_on_path(char* program){
    char* path = getenv("PATH");
    printf("%s\n", path);

    //TODO: Since I can't free() allocated resources while splitting $PATH
    //(because this leads to bugs), a more global approach is highly
    //recommended. Maybe reimplement this as a dinamic linked list, where
    //each value represents a dir.
    //Allocating more and more space at every command we give leads to high
    //waste of memory, and... splitting the $PATH everytime might be a bad
    //idea, anyway.
    char** splitted = strip(path);

    int i=0;
    while(1){
        char* dir = splitted[i];
        if(dir == NULL) break;

        printf(" %s \n", dir);
        i++;
    }
    printf("%d\n", i);
}

// https://www.delftstack.com/pt/howto/c/waitpid-in-c/
int spawn(char* program, char** arg_list){
    pid_t child_pid = fork();

    if (child_pid == -1){
        fprintf(stderr, "Unable to fork.\n");
        abort();
    }

    if (child_pid > 0){
        return child_pid;
    } else {
        execvp(program, arg_list);
        fprintf(stderr, "execve error.\n");
        abort();
    }
}

int check_builtin(char* full_command, char** stripped_command){
    int is_builtin = 0;

    char* command_fp = stripped_command[0];
    if (strcmp(command_fp, "exit") == 0){
        is_builtin = 1;
        printf("Quit.\n");
        exit(0);
    }
    else if (strcmp(command_fp, "cd") == 0){
        //Note that "cd a b c" puts the user in "a" directory.
        //It may be a good idea to tell the user that
        //"cd got too many arguments".

        is_builtin = 1;

        char* path;
        if(stripped_command[2] == NULL)
            path = getenv("HOME");
        else
            path = stripped_command[2];

        if(chdir(path) != 0)
            fprintf(stderr, "cd to %s failed.\n", path);
    }

    return is_builtin;
}

int main(int argc, char* argv[]){
    while(1){
        type_prompt();

        char* command = read_command();
        char** stripped_command = strip_command(command);

        if(!check_builtin(command, stripped_command)){
            //is_on_path(stripped_command[0]);

            int wstatus;
            pid_t child = spawn(stripped_command[0], &stripped_command[1]);

            if (waitpid(child, &wstatus, WUNTRACED | WCONTINUED) == -1) {
                fprintf(stderr, "Error");
                abort();
            }
        }
    }
}
